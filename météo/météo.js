// nominatim : pour les coordonnées GPS en fonction du nom du lieu
// https://nominatim.org/release-docs/latest/api/Search/#search-queries

// openweathermap : pour la météo en fonction des coordonnées GPS
// https://openweathermap.org/current

function getSaisie(){
    let saisie = document.getElementById("saisie").value;
    let gps = getGPS(saisie);
    // alert(gps);
}

function getGPS(saisie){
    let xhr = new XMLHttpRequest();
    let saisieArray = saisie.split(" ");
    let query = saisieArray[0];
    for (let i = 1; i<saisieArray.length; i++){
        query = query + "+" + saisieArray[i];
    }
    // extension RESTer pour tester les url
    let url = "https://nominatim.openstreetmap.org/search/" + query + "?format=json";
    xhr.open("GET",url);
    xhr.onreadystatechange = () => {
        let objet = JSON.parse(xhr.response); // = désérialisation du JSON
        let div = document.getElementById("lieux");
        div.replaceChildren();
        if (objet.length > 1){
            // TODO : faire en sorte que ce if fonctionne
            let pWarning = document.createElement("p");
            div.appendChild(pWarning);
            let textWarning = "Attention : plusieurs lieux ont été trouvés, veuillez préciser votre recherche ou cliquer sur l'un des liens ci-dessous";
            for (lieu of objet){
                // TODO : affiche une ligne par lieu avec un lien vers la météo de ce lieu
                let plat = document.createElement("p");
                let plon = document.createElement("p");
                div.appendChild(plat);
                div.appendChild(plon);
                let textlat = document.createTextNode(lieu.lat);
                let textlon = document.createTextNode(lieu.lon);
                plat.appendChild(textlat);
                plon.appendChild(textlon);
            }
        } else {
            // TODO : affiche directement la météo du lieu (mot-clés tests : semard nantes france)
        }


    }
    xhr.send();

    return url;
}