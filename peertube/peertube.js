function PeerTubeListVideos() {
    let xhr = new XMLHttpRequest();
    // extension RESTer pour tester les url
    xhr.open ("Get", "https://peertube.cpy.re/api/v1/videos");
    xhr.onreadystatechange = () => { PeerTubeListVideos_R(xhr);}
    xhr.send();
}
function PeerTubeListVideos_R(xhr){
    if (/*DONE*/ 4 == xhr.readyState){
        let obj = null;
        try {
            obj = JSON.parse(xhr.response); // = désérialisation du JSON
            // obj = xhr.responseXML // désérialisation du XML
        } catch (e) {
            console.log(e);
            return;
        }
        //console.log(obj.total)

        let div = document.getElementById("resultats");

        div.replaceChildren()

        let ul = document.createElement("ul");
        div.appendChild(ul) // pour dire où il doit apparaître
        for (film of obj.data) {
            let li = document.createElement("li");
            ul.appendChild(li);
            let text = document.createTextNode(film.name)
            li.appendChild(text);
            // TODO : li.setAttribute() ajout d'un href vers la vidéo
        }
    }
}
